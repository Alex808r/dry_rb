### 1 Dry-types and Dry-Struct
``` 
attribute :name, Types::Strict::String
attribute :age, Types::Age
attribute :height, Types::Coercible::Float
```

### 2 Dry-events create event publishers
```
register_event('character.created') # => on_character_created
register_event('character.removed') # => on_character_removed

def on_character_created(event)
  puts event.inspect

  @log.write "\nCharacter added: #{event[:character].inspect}\n"
end

def on_character_removed(event)
  puts event.inspect

  @log.write "\nCharacter removed: #{event[:character].inspect}\n"
end
```

### 3 Dry-validations
```
rule(:age) do
  key.failure('too low!') if value < 18
end

rule(:first_page_appear, :last_page_appear) do
  key(:last_page_appear).failure(:invalid) if values[:last_page_appear] < values[:first_page_appear]
end

rule(:height) do
  key.failure('wrong!') if key?(:height) && value < 30
end

rule(:email).validate(:email_format)

rule(:items).each do
  key.failure('is not valid') unless value.start_with?('I-')
end

rule(:relatives).each do |index:|
  unless %w[brother sister].include?(value[:relation])
    key([:relatives, :relation, index]).failure('invalid relation')
  end
end

rule do
  base.failure('base fail') unless custom_validator.valid?(values) # if rule_error?(:age) # base_rule_error?
end
```
### 4 Dry-monads
```
puts Maybe(item).maybe(&:id).value_or("not found") # => Some, None
res = Lotr.add_item_for_char 100, 42#, true
puts res.inspect
puts res.or("failed")
puts(res.trace) if res.failure?
```
