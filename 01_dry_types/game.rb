# frozen_string_literal: true

require 'dry-struct'
require 'dry-types'
require 'time'

module Types
  include Dry.Types()

  Age = Integer.constrained(gteq: 0) # тип Age - это число больше 0
  # id-12345
  ItemId = String.constrained(format: /\Aid-\d{2,5}\z/i)
end

class Character < Dry::Struct
  attribute :name, Types::Strict::String      # тип данных имя - Строка
  attribute :age, Types::Age                  # тип данных возраст - основан на типе Число
  attribute :height, Types::Coercible::Float  # пиведение типа данных к Float
end

bilbo = Character.new name: 'Bilbo', age: 50, height: '120.5'
aragorn = Character.new name: 'Aragorn', age: 73, height: 185

puts bilbo.inspect
puts bilbo.name
puts aragorn.inspect

# char_hash = Types::Hash.schema(
#   name: Types::Strict::String,
#   age: Types::Strict::Integer.
#     default(18).
#     constructor { |v|
#       v.nil? ? Dry::Types::Undefined : v
#     }
#   ).strict.with_key_transform(&:to_sym) # strict - неизвестные ключи

# puts char_hash["name" => "Bilbo", age: 20].inspect
# puts char_hash[name: "Bilbo", age: 20].inspect

class Item < Dry::Struct
  attribute :owner, Character
  attribute :description, Types::String.fallback('[EMPTY]') # fallback значение по умолчанию
  attribute :inscription, Types::Strict::String.optional        # optional значение атрибута может быть nil
  attribute :price, Types::Coercible::Float                     # приведение типа к Float
  attribute :found_at, Types::JSON::Time.default { Time.now }   # default если значение не указано
  attribute :locked, Types::Params::Bool                        # приведение типа из http params
  attribute :weight, Types::Strict::String | Types::Strict::Integer # тип либо строка либо число
  attribute :id, Types::ItemId
  attribute :quality, Types::Symbol.default(:common).enum(:common, :uncommon, :rare, :unique) # перечилены допустимые значения
  attribute :state?, Types::String.enum('good' => 0, 'damaged' => 1, 'broken' => 2) # ? - необязательный атрибут
  # { OWNER_NAME => YEAR }
  attribute :prev_owners?, Types::Hash.map(Types::String, Types::Integer)
  attribute :dmg?, Types.Instance(Range)
  attribute :tags?, Types::Array.of(Types::Coercible::String) # массив строк, приведение типа к строке
end

narsil = Item.new owner: aragorn,
                  description: 10, # некорректый тип => будет '[EMPTY]'
                  inscription: 'For the Dunedain!',
                  price: '10000.50',
                  found_at: '1567-11-26 12:00:00',
                  locked: true,
                  weight: '2kg',
                  id: 'id-123',
                  quality: :rare,
                  state: 'broken', # 2
                  dmg: (100..150),
                  tags: %i[sword dunedain magical]

ring = Item.new owner: bilbo,
                description: 'The One Ring',
                inscription: nil,
                price: 100_000_00.50,
                locked: 'true', # преобразует в true
                weight: 100,
                id: 'id-345',
                quality: :unique,
                prev_owners: { 'Sauron' => 500, 'Gollum' => 1000 },
                tags: %w[ring golden]

puts '=' * 50
puts narsil.inspect
puts "\nOwner of Narsil: #{narsil.owner.name}\n"
puts ring.inspect
